#include <cstdlib>
#include <cstdint>
#include <cassert>
#include <iostream>

using namespace std;

/** Vytvořte třídu CBitReader, která umožní číst z charového pole 
 *  (uint8_t* - pole unsigned integeru o osmi bitech, tj byte) 
 *  po bitech, nikoli po bytech.
*/
class CBitReader {
public:
    CBitReader ( uint8_t * array );
    bool next ( );

private:
    uint8_t * pntr;
	uint8_t currNum;
	int bitPos = 7;
	int index = 0;
};

CBitReader::CBitReader (uint8_t * array) {
	pntr = array;
	currNum = pntr[index];
}

bool CBitReader::next() {
	if ( bitPos == -1 ) {
		bitPos = 7;
		currNum = pntr[++index];	
	}
	/*	another possible way:
		currNum >>= 1;
		return currNum & 1;
	*/
	return (currNum & (1 << bitPos--));
}

int main ( ) {
    uint8_t * arr = new uint8_t[50];
    for ( int i = 0; i < 50; ++ i ) arr [ i ] = i;

    CBitReader br ( arr );
    for ( int i = 0; i < 8; i++ ) assert ( br.next ( ) == false ); // test 0
    for ( int i = 0; i < 7; i++ ) assert ( br.next ( ) == false ); // test 1
    assert ( br.next ( ) == true );
	for ( int i = 0; i < 6; i++ ) assert ( br.next ( ) == false ); // test 2
	assert ( br.next ( ) == true );
	assert ( br.next ( ) == false );
	for ( int i = 0; i < 6; i++ ) assert ( br.next ( ) == false ); // test 3
	assert ( br.next ( ) == true );
	assert ( br.next ( ) == true );
	for ( int i = 0; i < 5; i++ ) assert ( br.next ( ) == false ); // test 4
	assert ( br.next ( ) == true );
	assert ( br.next ( ) == false );
	assert ( br.next ( ) == false );
	for ( int i = 0; i < 5; i++ ) assert ( br.next ( ) == false ); // test 5
	assert ( br.next ( ) == true );
	assert ( br.next ( ) == false );
	assert ( br.next ( ) == true );
	for ( int i = 0; i < 5; i++ ) assert ( br.next ( ) == false ); // test 6
	assert ( br.next ( ) == true );
	assert ( br.next ( ) == true );
	assert ( br.next ( ) == false );
	
    delete [ ] arr;
    return 0;
}